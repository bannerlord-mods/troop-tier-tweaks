#!/bin/bash

MOD_DIR="Modules/TroopTierTweaks"
SOURCE_DIR="D:\\SteamLibrary\\steamapps\\common\\Mount & Blade II Bannerlord\\Modules\\TroopTierTweaks"

ZIP_FILE="TroopTierTweaks.zip"

generate_module_dir() {
    rm -rf "$MOD_DIR"
    rm -rf "$ZIP_FILE"

    mkdir -p "$MOD_DIR"

    # bin
    mkdir -p "$MOD_DIR/bin/Win64_Shipping_Client"
    cp -r "$SOURCE_DIR/bin/Win64_Shipping_Client/"* "$MOD_DIR/bin/Win64_Shipping_Client"

    # ModuleData
    mkdir -p "$MOD_DIR/ModuleData"
    cp -r "$SOURCE_DIR/ModuleData" "$MOD_DIR"

    # SubModule.xml
    cp -r "$SOURCE_DIR/SubModule.xml" "$MOD_DIR"
}

dotnet build

if [ $? -ne 0 ]; then
    echo "Build failed"
    exit 1
fi

if [ "$1" == "clean" ]; then
    rm -rf "$MOD_DIR"
fi

if [ "$1" == "package" ]; then
    generate_module_dir

    # Assets
    cp -r "./Resources/Assets" "$MOD_DIR"

    # GUI
    mkdir -p "$MOD_DIR/GUI"
    cp -r "$SOURCE_DIR/GUI/TroopTierTweaksSpriteData.xml" "$MOD_DIR/GUI"

    # AssetSources
    mkdir -p "$MOD_DIR/AssetSources"
    touch "$MOD_DIR/AssetSources/ThisDirectoryMustExistOtherwiseIconWillNotDisplay.txt"

    # RuntimeDataCache
    mkdir -p "$MOD_DIR/RuntimeDataCache"
    cp -r "./Resources/RuntimeDataCache" "$MOD_DIR"

    zip -r "$ZIP_FILE" "Modules"
fi

if [ "$1" == "modkit" ]; then
    generate_module_dir

    # Win64_Shipping_wEditor
    mkdir -p "$MOD_DIR/bin/Win64_Shipping_wEditor"
    cp -r "$SOURCE_DIR/bin/Win64_Shipping_wEditor/"* "$MOD_DIR/bin/Win64_Shipping_wEditor"

    # SpriteParts
    mkdir -p "$MOD_DIR/GUI/SpriteParts/ui_trooptiericons"
    cp -r "./Resources/GUI/SpriteParts/Config.xml" "$MOD_DIR/GUI/SpriteParts"

    # parse_sprite_data.py
    cp -r "parse_sprite_data.py" "$MOD_DIR"

    zip -r "$ZIP_FILE" "Modules"
fi