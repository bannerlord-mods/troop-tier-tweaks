#!/bin/bash

SOURCE_DIR="D:\\SteamLibrary\\steamapps\\common\\Mount & Blade II Bannerlord\\Modules\\TroopTierTweaks"
DEST_DIR="./Resources/"

mkdir -p "$DEST_DIR"

BACKUP_DIR=("Assets" "AssetSources" "GUI" "RuntimeDataCache")

for d in "${BACKUP_DIR[@]}"; do
    cp -r "$SOURCE_DIR/$d" "$DEST_DIR/"
done