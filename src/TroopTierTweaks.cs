﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using HarmonyLib;
using MCM.Abstractions.Base.Global;
using TaleWorlds.Library;
using TaleWorlds.MountAndBlade;
using TTT.Settings;


namespace TroopTierTweaks
{
    public class SubModule : MBSubModuleBase
    {
        public static List<Icon> IconList { get; private set; }
        public const int MaxTier = 20;

        protected override void OnSubModuleLoad()
        {
            try
            {
                base.OnSubModuleLoad();
                LoadIconDictionaryXml();

                Harmony harmony = new Harmony("com.TroopTierTweaks.bannerlord");
                harmony.PatchAll();
            }
            catch (Exception ex)
            {
                Debug.Print(string.Format("[TroopTierTweaks] Failed to apply patch.\n{0}", ex), 0, Debug.DebugColor.Blue);
            }
        }

        protected override void OnBeforeInitialModuleScreenSetAsRoot()
        {
            if (GlobalSettings<TTTSettings>.Instance != null)
            {
                InformationManager.DisplayMessage(new InformationMessage($"[TroopTierTweaks] {IconList.Count.ToString()} Icons Loaded", new Color(0f, 0f, 1f, 1f)));
            }
        }

        private static void LoadIconDictionaryXml()
        {
            try
            {
                string filePath = Path.Combine(BasePath.Name, "Modules", "TroopTierTweaks", "ModuleData", "icon_dictionary.xml");

                XmlSerializer serializer = new XmlSerializer(typeof(Icons));
                using (FileStream fs = new FileStream(filePath, FileMode.Open))
                {
                    Icons data = (Icons)serializer.Deserialize(fs);
                    IconList = data.IconList;
                }
            }
            catch (Exception ex)
            {
                Debug.Print(string.Format("[TroopTierTweaks] Failed to load icons.\n{0}", ex), 0, Debug.DebugColor.Blue);
            }
        }
    }
}