using MCM.Abstractions.Attributes;
using MCM.Abstractions.Attributes.v2;
using MCM.Abstractions.Base.Global;
using MCM.Common;
using TroopTierTweaks;

namespace TTT.Settings
{
    public class TTTSettings : AttributeGlobalSettings<TTTSettings>
    {
        private const int MinXPCost = 1000;
        private const int MaxXPCost = 1000000;

        public override string Id => "TTTSettings";
        public override string DisplayName => "Troop Tier Tweaks";
        public override string FolderName => "TroopTierTweaks";
        public override string FormatType => "json";

        public TTTSettings()
        {
            InitializeDropdown();
        }

        public void InitializeDropdown()
        {
            DefaultTierIcon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T1Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T2Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T3Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T4Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T5Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T6Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T7Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T8Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T9Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T10Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T11Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T12Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T13Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T14Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T15Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T16Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T17Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T18Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T19Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
            T20Icon = new Dropdown<Icon>(SubModule.IconList.ToArray(), 0);
        }

        [SettingPropertyInteger("Default XP Cost ", MinXPCost, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "Recommend 1700 or 2100. Vanilla T5 to T6 is 1700.")]
        [SettingPropertyGroup("Tier Upgrade Config", GroupOrder = 0)]
        public int DefaultXPCost { get; set; } = 2100;

        [SettingPropertyInteger("T1 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect. Vanilla: 100")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T1XPCost { get; set; } = 100;

        [SettingPropertyInteger("T2 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect. Vanilla: 300")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T2XPCost { get; set; } = 300;

        [SettingPropertyInteger("T3 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect. Vanilla: 550")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T3XPCost { get; set; } = 550;

        [SettingPropertyInteger("T4 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect. Vanilla: 900")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T4XPCost { get; set; } = 900;

        [SettingPropertyInteger("T5 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect. Vanilla: 1300")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T5XPCost { get; set; } = 1300;

        [SettingPropertyInteger("T6 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect. Vanilla: 1700")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T6XPCost { get; set; } = 1700;

        [SettingPropertyInteger("T7 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect.")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T7XPCost { get; set; } = 2100;

        [SettingPropertyInteger("T8 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect.")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T8XPCost { get; set; } = 2100;

        [SettingPropertyInteger("T9 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect.")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T9XPCost { get; set; } = 2100;
        
        [SettingPropertyInteger("T10 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect.")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T10XPCost { get; set; } = 2100;
        
        [SettingPropertyInteger("T11 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect.")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T11XPCost { get; set; } = 2100;
        
        [SettingPropertyInteger("T12 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect.")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T12XPCost { get; set; } = 2100;
        
        [SettingPropertyInteger("T13 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect.")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T13XPCost { get; set; } = 2100;
        
        [SettingPropertyInteger("T14 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect.")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T14XPCost { get; set; } = 2100;
        
        [SettingPropertyInteger("T15 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect.")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T15XPCost { get; set; } = 2100;
        
        [SettingPropertyInteger("T16 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect.")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T16XPCost { get; set; } = 2100;

        [SettingPropertyInteger("T17 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect.")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T17XPCost { get; set; } = 2100;

        [SettingPropertyInteger("T18 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect.")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T18XPCost { get; set; } = 2100;

        [SettingPropertyInteger("T19 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect.")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T19XPCost { get; set; } = 2100;

        [SettingPropertyInteger("T20 XP Cost", 0, MaxXPCost,  Order = 0, RequireRestart = false, HintText = "When set to 0, Default XP Cost will take effect.")]
        [SettingPropertyGroup("Tier Upgrade Config")]
        public int T20XPCost { get; set; } = 2100;



        [SettingPropertyDropdown("Default Icon Mapping", Order = 0, RequireRestart = false, HintText = "Tiers above 20")]
        [SettingPropertyGroup("Tier Icon Mapping", GroupOrder = 1)]
        public Dropdown<Icon> DefaultTierIcon { get; set; }

        [SettingPropertyDropdown("T1 Icon Mapping", Order = 1, RequireRestart = false, HintText = "T1 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T1Icon { get; set; }

        [SettingPropertyDropdown("T2 Icon Mapping", Order = 2, RequireRestart = false, HintText = "T2 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T2Icon { get; set; }

        [SettingPropertyDropdown("T3 Icon Mapping", Order = 3, RequireRestart = false, HintText = "T3 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T3Icon { get; set; }

        [SettingPropertyDropdown("T4 Icon Mapping", Order = 4, RequireRestart = false, HintText = "T4 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T4Icon { get; set; }

        [SettingPropertyDropdown("T5 Icon Mapping", Order = 5, RequireRestart = false, HintText = "T5 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T5Icon { get; set; }

        [SettingPropertyDropdown("T6 Icon Mapping", Order = 6, RequireRestart = false, HintText = "T6 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T6Icon { get; set; }

        [SettingPropertyDropdown("T7 Icon Mapping", Order = 7, RequireRestart = false, HintText = "T7 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T7Icon { get; set; }

        [SettingPropertyDropdown("T8 Icon Mapping", Order = 8, RequireRestart = false, HintText = "T8 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T8Icon { get; set; }

        [SettingPropertyDropdown("T9 Icon Mapping", Order = 9, RequireRestart = false, HintText = "T9 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T9Icon { get; set; }

        [SettingPropertyDropdown("T10 Icon Mapping", Order = 10, RequireRestart = false, HintText = "T10 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T10Icon { get; set; }

        [SettingPropertyDropdown("T11 Icon Mapping", Order = 11, RequireRestart = false, HintText = "T11 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T11Icon { get; set; }

        [SettingPropertyDropdown("T12 Icon Mapping", Order = 12, RequireRestart = false, HintText = "T12 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T12Icon { get; set; }

        [SettingPropertyDropdown("T13 Icon Mapping", Order = 13, RequireRestart = false, HintText = "T13 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T13Icon { get; set; }

        [SettingPropertyDropdown("T14 Icon Mapping", Order = 14, RequireRestart = false, HintText = "T14 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T14Icon { get; set; }

        [SettingPropertyDropdown("T15 Icon Mapping", Order = 15, RequireRestart = false, HintText = "T15 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T15Icon { get; set; }

        [SettingPropertyDropdown("T16 Icon Mapping", Order = 16, RequireRestart = false, HintText = "T16 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T16Icon { get; set; }

        [SettingPropertyDropdown("T17 Icon Mapping", Order = 17, RequireRestart = false, HintText = "T17 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T17Icon { get; set; }

        [SettingPropertyDropdown("T18 Icon Mapping", Order = 18, RequireRestart = false, HintText = "T18 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T18Icon { get; set; }

        [SettingPropertyDropdown("T19 Icon Mapping", Order = 19, RequireRestart = false, HintText = "T19 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T19Icon { get; set; }

        [SettingPropertyDropdown("T20 Icon Mapping", Order = 20, RequireRestart = false, HintText = "T20 Troops")]
        [SettingPropertyGroup("Tier Icon Mapping")]
        public Dropdown<Icon> T20Icon { get; set; }
    }
}