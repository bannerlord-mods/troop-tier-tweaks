using System.Linq;
using HarmonyLib;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.GameComponents;
using TaleWorlds.CampaignSystem.Party;
using TTT.Settings;

namespace TroopTierTweaks
{
    [HarmonyPatch(typeof(DefaultPartyTroopUpgradeModel), "GetXpCostForUpgrade")]
    public class GetXpCostForUpgradePatch
    {
        static bool Prefix(ref int __result, PartyBase party, CharacterObject characterObject, CharacterObject upgradeTarget)
        {
            if (upgradeTarget != null && characterObject.UpgradeTargets.Contains(upgradeTarget))
            {
                int tier = upgradeTarget.Tier;
                int num = 0;
                for (int i = characterObject.Tier + 1; i <= tier; i++)
                {
                    if (i <= 1)
                    {
                        if (TTTSettings.Instance.T1XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T1XPCost;
                    }
                    else if (i == 2)
                    {
                        if (TTTSettings.Instance.T2XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T2XPCost;
                    }
                    else if (i == 3)
                    {
                        if (TTTSettings.Instance.T3XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T3XPCost;
                    }
                    else if (i == 4)
                    {
                        if (TTTSettings.Instance.T4XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T4XPCost;
                    }
                    else if (i == 5)
                    {
                        if (TTTSettings.Instance.T5XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T5XPCost;
                    }
                    else if (i == 6)
                    {
                        if (TTTSettings.Instance.T6XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T6XPCost;
                    }
                    else if (i == 7)
                    {
                        if (TTTSettings.Instance.T7XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T7XPCost;
                    }
                    else if (i == 8)
                    {
                        if (TTTSettings.Instance.T8XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T8XPCost;
                    }
                    else if (i == 9)
                    {
                        if (TTTSettings.Instance.T9XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T9XPCost;
                    }
                    else if (i == 10)
                    {
                        if (TTTSettings.Instance.T10XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T10XPCost;
                    }
                    else if (i == 11)
                    {
                        if (TTTSettings.Instance.T11XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T11XPCost;
                    }
                    else if (i == 12)
                    {
                        if (TTTSettings.Instance.T12XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T12XPCost;
                    }
                    else if (i == 13)
                    {
                        if (TTTSettings.Instance.T13XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T13XPCost;
                    }
                    else if (i == 14)
                    {
                        if (TTTSettings.Instance.T14XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T14XPCost;
                    }
                    else if (i == 15)
                    {
                        if (TTTSettings.Instance.T15XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T15XPCost;
                    }
                    else if (i == 16)
                    {
                        if (TTTSettings.Instance.T16XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T16XPCost;
                    }
                    else if (i == 17)
                    {
                        if (TTTSettings.Instance.T17XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T17XPCost;
                    }
                    else if (i == 18)
                    {
                        if (TTTSettings.Instance.T18XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T18XPCost;
                    }
                    else if (i == 19)
                    {
                        if (TTTSettings.Instance.T19XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T19XPCost;
                    }
                    else if (i == 20)
                    {
                        if (TTTSettings.Instance.T20XPCost == 0)
                        {
                            num += TTTSettings.Instance.DefaultXPCost;
                        }
                        num += TTTSettings.Instance.T20XPCost;
                    }
                    else
                    {
                        num += TTTSettings.Instance.DefaultXPCost;
                    }
                }
                __result = num;
                return false;
            }
            __result = 100000000;
            return false;
            
        }
    }
}