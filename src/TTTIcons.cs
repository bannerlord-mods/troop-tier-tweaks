using System.Collections.Generic;
using System.Xml.Serialization;

namespace TTT.Settings
{
    [XmlRoot("Icons")]
    public class Icons
    {
        [XmlElement("Icon")]
        public List<Icon> IconList { get; set; }
    }

    public class Icon
    {
        [XmlAttribute("Name")]
        public string Name { get; set; }
        [XmlAttribute("Value")]
        public string Value { get; set; }

        public override string ToString() => Name;
    }
}