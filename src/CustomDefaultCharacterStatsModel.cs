using HarmonyLib;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.GameComponents;
using TaleWorlds.Library;

namespace TroopTierTweaks
{
    [HarmonyPatch(typeof(DefaultCharacterStatsModel), "GetTier")]
    public class GetTierPatch
    {
        static bool Prefix(ref int __result, CharacterObject character)
        {
            if (character.IsHero)
            {
                __result = 0;
                return false;
            }
            __result = MathF.Min(MathF.Max(MathF.Ceiling(((float)character.Level - 5f) / 5f), 0), SubModule.MaxTier);
            return false;
        }
    }
}
