using System.Collections.Generic;
using HarmonyLib;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.ViewModelCollection;
using TaleWorlds.Core;
using TaleWorlds.Core.ViewModelCollection.Generic;
using TaleWorlds.Localization;
using TTT.Settings;

namespace TroopTierTweaks
{
    [HarmonyPatch(typeof(CampaignUIHelper), "GetCharacterTierData")]
    public class GetCharacterTierDataPatch
    {
        static bool Prefix(ref StringItemWithHintVM __result, CharacterObject character, bool isBig = false)
        {
            int tier = character.Tier;
            if (tier <= 0 || tier > SubModule.MaxTier)
            {
                __result = new StringItemWithHintVM("", TextObject.Empty);
                return false;
            }

            GameTexts.SetVariable("TIER_LEVEL", tier);
            TextObject hint = new TextObject("{=!}" + GameTexts.FindText("str_party_troop_tier", null).ToString(), null);

            string text = GetTierIconPath(tier, isBig);

            __result = new StringItemWithHintVM(text, hint);
            return false;
        }

        private static string GetTierIconPath(int tier, bool isBig)
        {
            if (tier > 0)
            {
                return TierIconMCMMapping(tier);
            }
            string str = isBig ? $"{tier}_big" : tier.ToString();
            return $"General\\TroopTierIcons\\icon_tier_{str}";
        }

        private static string TierIconMCMMapping(int tier)
        {
            var tierIcons = new Dictionary<int, string>
            {
                { 1, TTTSettings.Instance.T1Icon.SelectedValue.Value },
                { 2, TTTSettings.Instance.T2Icon.SelectedValue.Value },
                { 3, TTTSettings.Instance.T3Icon.SelectedValue.Value },
                { 4, TTTSettings.Instance.T4Icon.SelectedValue.Value },
                { 5, TTTSettings.Instance.T5Icon.SelectedValue.Value },
                { 6, TTTSettings.Instance.T6Icon.SelectedValue.Value },
                { 7, TTTSettings.Instance.T7Icon.SelectedValue.Value },
                { 8, TTTSettings.Instance.T8Icon.SelectedValue.Value },
                { 9, TTTSettings.Instance.T9Icon.SelectedValue.Value },
                { 10, TTTSettings.Instance.T10Icon.SelectedValue.Value },
                { 11, TTTSettings.Instance.T11Icon.SelectedValue.Value },
                { 12, TTTSettings.Instance.T12Icon.SelectedValue.Value },
                { 13, TTTSettings.Instance.T13Icon.SelectedValue.Value },
                { 14, TTTSettings.Instance.T14Icon.SelectedValue.Value },
                { 15, TTTSettings.Instance.T15Icon.SelectedValue.Value },
                { 16, TTTSettings.Instance.T16Icon.SelectedValue.Value },
                { 17, TTTSettings.Instance.T17Icon.SelectedValue.Value },
                { 18, TTTSettings.Instance.T18Icon.SelectedValue.Value },
                { 19, TTTSettings.Instance.T19Icon.SelectedValue.Value },
                { 20, TTTSettings.Instance.T20Icon.SelectedValue.Value }
            };

            string icon = tierIcons.ContainsKey(tier) ? tierIcons[tier] : TTTSettings.Instance.DefaultTierIcon.SelectedValue.Value;
            return $"General\\TroopTierIcons\\{icon}";
        }
    }
}