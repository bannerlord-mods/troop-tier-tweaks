using System;
using HarmonyLib;
using Helpers;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.CampaignBehaviors;
using TaleWorlds.CampaignSystem.Party;
using TaleWorlds.CampaignSystem.Roster;

namespace TroopTierTweaks
{
    [HarmonyPatch(typeof(DesertionCampaignBehavior), "PartiesCheckDesertionDueToPartySizeExceedsPaymentRatio")]
    public class PartiesCheckDesertionDueToPartySizeExceedsPaymentRatioPatch
    {
        static bool Prefix(MobileParty mobileParty, ref TroopRoster desertedTroopList)
        {
            int partySizeLimit = mobileParty.Party.PartySizeLimit;

            if ((mobileParty.IsLordParty || mobileParty.IsCaravan) &&
                mobileParty.Party.NumberOfAllMembers > partySizeLimit &&
                mobileParty != MobileParty.MainParty &&
                mobileParty.MapEvent == null)
            {
                int excessMembers = mobileParty.Party.NumberOfAllMembers - partySizeLimit;
                for (int i = 0; i < excessMembers; i++)
                {
                    CharacterObject characterToDesert = GetLowestLevelNonHeroCharacter(mobileParty);

                    if (characterToDesert != null)
                    {
                        bool isWounded = mobileParty.MemberRoster.GetElementWoundedNumber(mobileParty.MemberRoster.FindIndexOfTroop(characterToDesert)) > 0;
                        mobileParty.MemberRoster.AddToCounts(characterToDesert, -1, false, isWounded ? -1 : 0, 0, true, -1);
                    }
                }
            }

            if (mobileParty.IsWageLimitExceeded() || mobileParty.Party.NumberOfAllMembers > mobileParty.LimitedPartySize)
            {
                int numberOfDeserters = Campaign.Current.Models.PartyDesertionModel.GetNumberOfDeserters(mobileParty);
                DesertExcessTroops(mobileParty, numberOfDeserters, ref desertedTroopList);
            }
            return false;
        }

        private static CharacterObject GetLowestLevelNonHeroCharacter(MobileParty mobileParty)
        {
            CharacterObject lowestLevelCharacter = mobileParty.MapFaction.BasicTroop;
            int lowestLevel = int.MaxValue;

            for (int j = 0; j < mobileParty.MemberRoster.Count; j++)
            {
                CharacterObject character = mobileParty.MemberRoster.GetCharacterAtIndex(j);
                int characterCount = mobileParty.MemberRoster.GetElementNumber(j);
                
                if (!character.IsHero && character.Level < lowestLevel && characterCount > 0)
                {
                    lowestLevel = character.Level;
                    lowestLevelCharacter = character;
                }
            }
            return lowestLevel < int.MaxValue ? lowestLevelCharacter : null;
        }

        private static void DesertExcessTroops(MobileParty mobileParty, int numberOfDeserters, ref TroopRoster desertedTroopList)
        {
            int desertedCount = 0;
            while (desertedCount < numberOfDeserters && mobileParty.MemberRoster.TotalRegulars > 0)
            {
                int stackIndex = -1;
                int lowestTier = int.MaxValue;
                int desertersInStack = 1;

                for (int i = 0; i < mobileParty.MemberRoster.Count; i++)
                {
                    CharacterObject character = mobileParty.MemberRoster.GetCharacterAtIndex(i);
                    int characterCount = mobileParty.MemberRoster.GetElementNumber(i);

                    if (!character.IsHero && characterCount > 0 && character.Tier < lowestTier)
                    {
                        lowestTier = character.Tier;
                        stackIndex = i;
                        desertersInStack = Math.Min(characterCount, numberOfDeserters - desertedCount);
                    }
                }

                if (stackIndex >= 0)
                {
                    MobilePartyHelper.DesertTroopsFromParty(mobileParty, stackIndex, desertersInStack, 0, ref desertedTroopList);
                    desertedCount += desertersInStack;
                }
            }
        }
    }
}