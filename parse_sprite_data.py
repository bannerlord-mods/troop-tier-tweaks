import os
import sys
import re
import xml.etree.ElementTree as ET
import xml.dom.minidom as minidom

sprite_dir = "GUI"
sprite_file = "TroopTierTweaksSpriteData.xml"

dictionary_dir = "ModuleData"
dictionary_file = "icon_dictionary.xml"

pre_text = "General\\TroopTierIcons\\"
sprite_names = []

def get_filepath(filedir, filename):
    if os.path.exists(filedir):
        file_path = os.path.join(filedir, filename)
        return file_path if os.path.isfile(file_path) else ""
    return filename if os.path.isfile(filename) else ""

def create_dictionary_if_not_exist():
    global dictionary_file
    if not dictionary_file:
        dictionary_file = "icon_dictionary.xml" if not os.path.exists(dictionary_dir) else os.path.join(dictionary_dir, "icon_dictionary.xml")

def regex_add_pre_text(content, text):
    return re.sub(fr"([^\\])({text})", f"\\1{pre_text.replace('\\', r'\\')}\\2", content)

def get_sprite_names():
    global sprite_names
    tree = ET.parse(sprite_file)
    root = tree.getroot()

    for e in root.findall('.//SpritePart'):
        if pre_text not in e.find('Name').text:
            sprite_names.append((e.find('Name').text, False))
            continue
        sprite_names.append((e.find('Name').text.split('\\')[-1], True))
    
    print(f"Found {len(sprite_names)} sprite definitions...")

    def extract_number(icon_name):
        match = re.search(r'(\d+)', icon_name)
        return int(match.group(1)) if match else 0
    
    sprite_names = sorted(sprite_names, key=lambda x: extract_number(x[0]))

def parse_sprite_data():
    with open(sprite_file, 'r') as f:
        content = f.read()
        
        for s in sprite_names:
            if s[1] is False:
                content = regex_add_pre_text(content, s[0])

    with open(sprite_file, 'w') as f:
        f.write(content)

def build_sprite_dictionary():
    root = ET.Element("Icons")
    
    for i in range(len(sprite_names)):
        icon = ET.SubElement(root, "Icon")
        icon.set("Name", f"{sprite_names[i][0]}")
        icon.set("Value", f"{sprite_names[i][0]}")

    formatted_str = ET.tostring(root, "utf-8")
    formatted_str = minidom.parseString(formatted_str)
    formatted_str = formatted_str.toprettyxml(indent=f"{" " * 4}")

    with open(dictionary_file, "w", encoding="utf-8") as f:
        f.write(formatted_str)

sprite_file = get_filepath(sprite_dir, sprite_file)
if not sprite_file:
    print("Sprite data xml not found, please generate sprite data xml")
    sys.exit(1)

dictionary_file = get_filepath(dictionary_dir, dictionary_file)
create_dictionary_if_not_exist()

get_sprite_names()
parse_sprite_data()
print("Sprite Data Parsed...")
build_sprite_dictionary()
print("Dictionary Built...")
print("MCM Dropdown options will display sprite data name...")
print("Please change icon_dictionary.xml:Icon.Name for MCM dropdown options display name...")
